﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core
{
    public enum RemoteUpdateKind
    {
        /// <summary>
        /// URL下载更新
        /// </summary>
        Url,

        /// <summary>
        /// 文件上传更新
        /// </summary>
        File
    }
}
